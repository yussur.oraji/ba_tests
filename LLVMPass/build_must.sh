#!/usr/bin/env bash
set -e

# Script by Simon Schwitanski, published at https://github.com/RWTH-HPC/must-rma-correctness22-supplemental
# with no license
# This version has been slightly edited.

## Configuration variables
### MUST repo location
MUST_REPO_DIR=$(pwd)/MUST

### Build and install directories
BUILD_DIR=$(pwd)/MUST_build/
INSTALL_DIR=$(pwd)/MUST_install/

CMAKE_FLAGS="-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_TYPEART=OFF -DENABLE_FORTRAN=OFF -DENABLE_TESTS=OFF -DTESTS_WORKERS=1"

echo "= Configuration ="
echo "MUST_REPO_DIR=$MUST_REPO_DIR"
echo "BUILD_DIR=$BUILD_DIR"
echo "INSTALL_DIR=$INSTALL_DIR"
echo "CMAKE_FLAGS=$CMAKE_FLAGS"

# Finally configure and build MUST
mkdir -p $BUILD_DIR
mkdir -p $INSTALL_DIR
cd $BUILD_DIR
cmake $CMAKE_FLAGS $MUST_REPO_DIR
make -j$(nproc) install install-prebuilds