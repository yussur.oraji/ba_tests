#!/usr/bin/env zsh
set -e

# Get user config
source passtest_user.sh

# JUBE has to use zsh to work with module system on CLAIX Rocky
export JUBE_EXEC_SHELL=$(which zsh)

# Load required modules
module load Clang iimpi JUBE

$jube_bin run jubefile_correctness.yaml
$jube_bin result tests/jube_correctness_out -i last

$jube_bin run jubefile_performance.yaml --tag hpc
$jube_bin result tests/jube_performance_out -i last
