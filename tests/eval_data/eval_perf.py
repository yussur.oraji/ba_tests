#!/usr/bin/python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

# Predefined colors
color_rwth_darkblue = '#0053a0'
color_rwth_lightblue = '#8fbbe6'
color_i12_medyellow = '#fabe50'

### USER GRAPH CONFIGURATION START ###

# Output file format ending
fileformat = ".pdf"

# Size of absolute runtime and slowdown graphs
graph_size_perrank = (4,4)
# Size of separate opts and delay-full-kernelonly graphs
graph_size_pertest = (5,4)

# Number of columns in legend for runtime and slowdown graphs
graph_legend_ncols_perrank = 2
# Number of columns in legend for separate opts and delay-full-kernelonly graphs
graph_legend_ncols_pertest = 3

# Color preset to use, may be 'presentation', 'thesis' or 'none'. Takes precedence over manual input.
color_preset = 'thesis'

# Color used for absolute runtime 'base' measure, or 'none' in slowdown graph
color_base = 'white'
# Color used for TSan execution without MUST. Also used for whitelist in sepopts graph
color_tsan = 'dimgrey'
# Color used for TSan execution with MUST. Also used for delayed detection in sepopts graph
color_must = 'gainsboro'

# Hatching on unoptimized instrumentation (Only absolute runtime, slowdown graphs)
hatch_std = ''
# Hatching on optimized instrumentation (Only absolute runtime, slowdown graphs). Also used for ext whitelist in sepopts graph and full runtime in delay only graph
hatch_opt = '//'
# Hatching for separate optimization graph for when all are applied
hatch_sepopt_allopts = '..'

# Legends to draw. Bool list, order: stencil, transpose, minimd, minivite
legends_runtime = [True, False, True, False]
legends_slowdown = [True, False, True, False]

###  USER GRAPH CONFIGURATION END  ###

plt.rcParams['pdf.fonttype'] = 42

if color_preset == 'presentation':
    color_base = color_i12_medyellow
    color_tsan = color_rwth_darkblue
    color_must = color_rwth_lightblue
elif color_preset == 'thesis':
    color_base = 'white'
    color_tsan = 'dimgrey'
    color_must = 'gainsboro'
elif color_preset != 'none':
    print("ERR: color_preset invalid!")
    exit(1)

# Load data
data = pd.read_csv("perf_result.csv")

# Remove JUBE related data
data = data.drop(["jube_wp_id"], axis=1)

# Prepare data
offsets = [1,3,5]
def toRange(x):
    if x == 48: return 1
    if x == 96: return 3
    if x == 192: return 5

data.ranks = data.ranks.map(toRange)

base = "noinst+direct+allinst+immediate+notyping"
must_std = "tsan+must+allinst+immediate+notyping"
must_opt = "tsan+must+whitelist+delayed+utilizetypes"
direct_std = "tsan+direct+allinst+immediate+notyping"
direct_opt = "tsan+direct+whitelist+delayed+utilizetypes"

direct_whitelist = "tsan+direct+whitelist+immediate+notyping"
direct_delay = "tsan+direct+allinst+delayed+notyping"
direct_extwhitelist = "tsan+direct+whitelist+immediate+utilizetypes"

def create_categories(data, testcase):
    data = data[data["test_name"] == testcase]

    # Prepare categories
    catdata = {}
    width = 0
    for i in data['use_tsan'].unique():
        tempdata_1 = data[data['use_tsan'] == i]
        for j in data['use_must'].unique():
            tempdata_2 = tempdata_1[tempdata_1['use_must'] == j]
            for k in data['use_whitelist'].unique():
                tempdata_3 = tempdata_2[tempdata_2['use_whitelist'] == k]
                for l in data['use_activator'].unique():
                    tempdata_4 = tempdata_3[tempdata_3['use_activator'] == l]
                    for m in data['use_remoteaccesstype'].unique():
                        tempdata_5 = tempdata_4[tempdata_4['use_remoteaccesstype'] == m]
                        if tempdata_5.empty: continue
                        catdata[i+"+"+j+"+"+k+"+"+l+"+"+m] = tempdata_5.set_index("ranks")
    return catdata

stencildata = create_categories(data, "stencil_ok")
transposedata = create_categories(data, "transpose_ok")
minimddata = create_categories(data, "minimd_ok")
minivitedata = create_categories(data, "minivite_ok")

def create_runtime_graph(catdata, testcase, dolegend):
    fig = plt.figure(figsize=graph_size_perrank)

    # Prepare Graph
    fig.suptitle(testcase + " absolute runtime per rank")

    plt.xlabel("Ranks")
    plt.ylabel("Runtime Average (s)")

    plt.bar(catdata[base].index, catdata[base].runtime_avg, label="base", width=0.3, edgecolor='black', color=color_base)
    plt.errorbar(catdata[base].index, catdata[base].runtime_avg, yerr=catdata[base].runtime_std, fmt=".k")

    plt.bar(catdata[direct_std] .index + 0.4, catdata[direct_std] .runtime_avg, label="tsan_std", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_std)
    plt.errorbar(catdata[direct_std].index + 0.4, catdata[direct_std].runtime_avg, yerr=catdata[direct_std].runtime_std, fmt=".k")
    plt.bar(catdata[direct_opt] .index + 0.7, catdata[direct_opt] .runtime_avg, label="tsan_opt", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_opt)
    plt.errorbar(catdata[direct_opt].index + 0.7, catdata[direct_opt].runtime_avg, yerr=catdata[direct_opt].runtime_std, fmt=".k")

    plt.bar(catdata[must_std] .index + 1.1, catdata[must_std] .runtime_avg, label="must_std", width=0.3, edgecolor='black', color=color_must, hatch=hatch_std)
    plt.errorbar(catdata[must_std].index + 1.1, catdata[must_std].runtime_avg, yerr=catdata[must_std].runtime_std, fmt=".k")
    plt.bar(catdata[must_opt] .index + 1.4, catdata[must_opt] .runtime_avg, label="must_opt", width=0.3, edgecolor='black', color=color_must, hatch=hatch_opt)
    plt.errorbar(catdata[must_opt].index + 1.4, catdata[must_opt].runtime_avg, yerr=catdata[must_opt].runtime_std, fmt=".k")

    width = 1.4

    finish_graph(width, fig, testcase, "runtime", dolegend)

def finish_graph(width, fig, testcase, graphtype, dolegend):
    # Finish and save graph
    jep = offsets.copy()
    jep = list(map(lambda x: x + width/2, jep))
    plt.xticks(jep, ("48","96","192"))
    if dolegend:
        plt.legend(bbox_to_anchor=(0.5,1.2), loc="lower center", ncols=graph_legend_ncols_perrank)
    fig.savefig(testcase + "_" + graphtype + fileformat, dpi=300, bbox_inches="tight")
    #plt.show()
    plt.close()

create_runtime_graph(stencildata, "stencil_ok", legends_runtime[0])
create_runtime_graph(transposedata, "transpose_ok", legends_runtime[1])
create_runtime_graph(minimddata, "minimd_ok", legends_runtime[2])
create_runtime_graph(minivitedata, "minivite_ok", legends_runtime[3])

def create_slowdown_graph(catdata, testcase, dolegend):
    fig = plt.figure(figsize=graph_size_perrank)
    # Prepare Graph
    fig.suptitle(testcase + " slowdown per rank")

    plt.xlabel("Ranks")
    plt.ylabel("Slowdown factor")

    plt.axhline(y=1, xmin=0, xmax=999, color='black', label='base')

    plt.bar(catdata[direct_std].index, catdata[direct_std].runtime_avg.div(catdata[base].runtime_avg), label="tsan_std", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_std)
    plt.bar(catdata[direct_opt].index + 0.3, catdata[direct_opt].runtime_avg.div(catdata[base].runtime_avg), label="tsan_opt", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_opt)

    plt.bar(catdata[must_std].index + 0.7, catdata[must_std].runtime_avg.div(catdata[base].runtime_avg), label="must_std", width=0.3, edgecolor='black', color=color_must, hatch=hatch_std)
    plt.bar(catdata[must_opt].index + 1.0, catdata[must_opt].runtime_avg.div(catdata[base].runtime_avg), label="must_opt", width=0.3, edgecolor='black', color=color_must, hatch=hatch_opt)

    width = 1.1

    # Finish and save graph
    finish_graph(width, fig, testcase, "slowdown", dolegend)

create_slowdown_graph(stencildata, "stencil_ok", legends_slowdown[0])
create_slowdown_graph(transposedata, "transpose_ok", legends_slowdown[1])
create_slowdown_graph(minimddata, "minimd_ok", legends_slowdown[2])
create_slowdown_graph(minivitedata, "minivite_ok", legends_slowdown[3])

# Prepare for separate opts graph
sepdata = data[data["ranks"] == toRange(192)]
sepdata = sepdata.drop(["ranks"], axis=1)

def toRange2(x):
    if x == "stencil_ok": return 1
    if x == "transpose_ok": return 3
    if x == "minimd_ok": return 5
    if x == "minivite_ok": return 7

# Prepare categories
catdata = {}
for i in sepdata['use_tsan'].unique():
    tempdata_1 = sepdata[sepdata['use_tsan'] == i]
    for j in sepdata['use_must'].unique():
        tempdata_2 = tempdata_1[tempdata_1['use_must'] == j]
        for k in sepdata['use_whitelist'].unique():
            tempdata_3 = tempdata_2[tempdata_2['use_whitelist'] == k]
            for l in sepdata['use_activator'].unique():
                tempdata_4 = tempdata_3[tempdata_3['use_activator'] == l]
                for m in sepdata['use_remoteaccesstype'].unique():
                    tempdata_5 = tempdata_4[tempdata_4['use_remoteaccesstype'] == m]
                    if tempdata_5.empty: continue
                    tempdata_5.test_name = tempdata_5.test_name.map(toRange2)
                    catdata[i+"+"+j+"+"+k+"+"+l+"+"+m] = tempdata_5
                    catdata[i+"+"+j+"+"+k+"+"+l+"+"+m] = catdata[i+"+"+j+"+"+k+"+"+l+"+"+m].set_index("test_name")



# Prepare sepopts graph
fig = plt.figure(figsize=graph_size_pertest)
fig.suptitle("Slowdown per testcase on 192 ranks, separate optimizations")

plt.xlabel("Testcase")
plt.ylabel("Slowdown factor")

plt.axhline(y=1, xmin=0, xmax=999, color='black', label='base')

plt.bar(catdata[direct_std].index, catdata[direct_std].runtime_avg.div(catdata[base].runtime_avg), label="none", width=0.3, edgecolor='black', color=color_base)
plt.bar(catdata[direct_std].index + 0.3, catdata[direct_whitelist].runtime_avg.div(catdata[base].runtime_avg), label="whitelist", width=0.3, edgecolor='black', color=color_tsan)
plt.bar(catdata[direct_std].index + 0.6, catdata[direct_extwhitelist].runtime_avg.div(catdata[base].runtime_avg), label="extwhitelist", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_opt)
plt.bar(catdata[direct_std].index + 0.9, catdata[direct_delay].runtime_avg.div(catdata[base].runtime_avg), label="delaydetect", width=0.3, edgecolor='black', color=color_must)
plt.bar(catdata[direct_std].index + 1.2, catdata[direct_opt].runtime_avg.div(catdata[base].runtime_avg), label="all", width=0.3, edgecolor='black', color=color_must, hatch=hatch_sepopt_allopts)
width = 1.2
jep = [1,3,5,7]
jep = list(map(lambda x: x + width/2, jep))
plt.xticks(jep, ("stencil", "transpose", "minimd", "minivite"))
plt.legend(bbox_to_anchor=(0.5,1.2), loc="lower center", ncols=graph_legend_ncols_pertest)
fig.savefig("separateopts" + fileformat, dpi=300, bbox_inches="tight")
plt.close()

# Prepare delay graph
fig = plt.figure(figsize=graph_size_pertest)
fig.suptitle("Slowdown per testcase on 192 ranks for the delayed detection approach")

plt.xlabel("Testcase")
plt.ylabel("Slowdown factor")

plt.axhline(y=1, xmin=0, xmax=999, color='black', label='base')

plt.bar(catdata[direct_std].index + 0.3, catdata[direct_delay].runtime_avg.div(catdata[base].runtime_avg), label="delay (kernel only)", width=0.3, edgecolor='black', color=color_tsan)
plt.bar(catdata[direct_std].index + 0.6, catdata[direct_delay].full_runtime_avg.div(catdata[base].full_runtime_avg), label="delay (full runtime)", width=0.3, edgecolor='black', color=color_tsan, hatch=hatch_opt)

width = 1.2
jep = [1,3,5,7]
jep = list(map(lambda x: x + width/2, jep))
plt.xticks(jep, ("stencil", "transpose", "minimd", "minivite"))
plt.legend(bbox_to_anchor=(0.5,1.2), loc="lower center", ncols=graph_legend_ncols_pertest)
fig.savefig("delaycompare" + fileformat, dpi=300, bbox_inches="tight")
plt.close()
