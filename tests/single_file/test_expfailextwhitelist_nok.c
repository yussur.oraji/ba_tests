#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define ARR_SIZE 10
#define BUF_SIZE sizeof(int)*ARR_SIZE

void rank0(int*);
void rank1(int*);
void rankdef();

MPI_Win window;

__attribute__((noinline)) void deeeeep(int* x) {
    MPI_Get(x, ARR_SIZE, MPI_INT, 1, 0, ARR_SIZE, MPI_INT, window);
}

__attribute__((noinline)) void deeeep(int* x) { deeeeep(x); }
__attribute__((noinline)) void deeep(int* x) { deeeep(x); }
__attribute__((noinline)) void deep(int* x) { deeep(x); }

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int *buf;
    MPI_Win_allocate(BUF_SIZE, sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &buf, &window);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Win_fence(0, window);
    switch (rank) {
        case 0:
            MPI_Barrier(MPI_COMM_WORLD);
            rank0(buf);
            break;
        case 1:
            rank1(buf);
            MPI_Barrier(MPI_COMM_WORLD);
            break;
        default:
            rankdef();
            break;

    }
    MPI_Win_fence(0, window);
    MPI_Win_free(&window);
    MPI_Finalize();
}

void rank0(int* buf) {
    deep(buf);
    for (int i = 0; i < ARR_SIZE; i++) {
        printf("R0: buf[%d] = %d\n", i, buf[i]);
    }
}

void rank1(int* buf) {
    for (int i = 0; i < ARR_SIZE; i++) {
        buf[i] = i;
    }
}

void rankdef() {
    //MPI_Win_fence(0, window);
}