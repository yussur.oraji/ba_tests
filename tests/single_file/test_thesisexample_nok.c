#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int rank;
    int *buf;
    int *test = malloc(sizeof(int));
    MPI_Win win;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Win_allocate(sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &buf, &win);
    MPI_Win_fence(0, win);
    if (rank == 0) {
        test[0] = 42;
        MPI_Put(test, 1, MPI_INT, 1, 0, 1, MPI_INT, win);
    } else if (rank == 1) {
        printf("Buffer: %d\n", *buf);
    }
    MPI_Win_fence(0,win);
    free(test);
    MPI_Win_free(&win);
    MPI_Finalize();
}