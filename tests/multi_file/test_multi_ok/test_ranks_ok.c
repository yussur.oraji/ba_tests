#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define ARR_SIZE 10
#define BUF_SIZE sizeof(int)*ARR_SIZE

extern MPI_Win window;

extern int buf[ARR_SIZE];

void rank0() {
    MPI_Get(&buf, ARR_SIZE, MPI_INT, 1, 0, ARR_SIZE, MPI_INT, window);
    MPI_Win_fence(0, window);
    for (int i = 0; i < ARR_SIZE; i++) {
        printf("R0: buf[%d] = %d\n", i, buf[i]);
    }
}

void rank1() {
    for (int i = 0; i < ARR_SIZE; i++) {
        buf[i] = i;
    }
}

void rankdef() {
    //MPI_Win_fence(0, window);
}