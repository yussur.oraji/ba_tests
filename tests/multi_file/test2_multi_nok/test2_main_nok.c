#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#define ARR_SIZE 10
#define BUF_SIZE sizeof(int)*ARR_SIZE

void rank0();
void rank1();
void rankdef();

MPI_Win window;

int buf[ARR_SIZE] = {0};

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Win_create(buf, BUF_SIZE, sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &window);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Win_fence(0, window);
    switch (rank) {
        case 0:
            rank0();
            break;
        case 1:
            rank1();
            break;
        default:
            rankdef();
            break;

    }
    MPI_Win_fence(0, window);
    MPI_Win_free(&window);
    MPI_Finalize();
}