FROM ubuntu:22.04

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

USER root
RUN apt-get update
RUN apt-get -y -qq install software-properties-common
RUN add-apt-repository ppa:ubuntu-toolchain-r/test
RUN apt-get update --fix-missing && apt-get -y -qq install autoconf automake autotools-dev build-essential clang-13 clang-tools-13 cmake cvc4 \
                           gcc-10 llvm-13-linker-tools git mpich libboost-dev libboost-context-dev libcairo2 libdw-dev \
                           libelf-dev libevent-dev libllvm13 libncurses5 libunwind-dev libtinfo-dev \
                           libtool libxml2-dev libz3-dev llvm-13 llvm-13-dev lsof psmisc \
                           nano python3-jinja2 python3-pip quilt valgrind wget z3 zlib1g-dev && \
    apt-get autoremove -yq && \
    apt-get clean -yq

RUN pip3 install http://apps.fz-juelich.de/jsc/jube/jube2/download.php?version=latest --user

# Get repo
RUN mkdir -p -m 0600 ~/.ssh
RUN ssh-keyscan github.com >> ~/.ssh/known_hosts
RUN ssh-keyscan git-ce.rwth-aachen.de >> ~/.ssh/known_hosts
RUN --mount=type=ssh git clone --recursive git@git-ce.rwth-aachen.de:yussur.oraji/BA_tests --depth=1 --shallow-submodules

# Compile LLVM
WORKDIR /BA_tests/LLVMPass/llvm-newpass/build
RUN cmake ../llvm -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS="clang;compiler-rt" -DCMAKE_INSTALL_PREFIX=../install
RUN cmake --build . -j$(nproc)

# Compile MUST, with workaround to allow for shallow copy
WORKDIR /BA_tests/LLVMPass/
RUN echo "0.0.0" | tee MUST/.version MUST/externals/GTI/.version MUST/externals/GTI/externals/PnMPI/.version
RUN rm -rf MUST/.git MUST/externals/GTI/.git MUST/externals/GTI/externals/PnMPI/.git
RUN ./build_must.sh

WORKDIR /BA_tests