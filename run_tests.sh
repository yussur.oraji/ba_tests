#!/bin/bash
set -e

# Get user config
source passtest_user.sh

$jube_bin run jubefile_correctness.yaml
$jube_bin result tests/jube_correctness_out -i last

$jube_bin run jubefile_performance.yaml
$jube_bin result tests/jube_performance_out -i last
